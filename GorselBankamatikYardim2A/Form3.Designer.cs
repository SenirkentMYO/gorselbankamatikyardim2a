﻿namespace GorselBankamatikYardim2A
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Btnparacekme = new System.Windows.Forms.Button();
            this.Btnkart = new System.Windows.Forms.Button();
            this.Btnyatirma = new System.Windows.Forms.Button();
            this.Btntransfer = new System.Windows.Forms.Button();
            this.Btngiris = new System.Windows.Forms.Button();
            this.Btnhesap = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Lblsoyad = new System.Windows.Forms.Label();
            this.Lblad = new System.Windows.Forms.Label();
            this.Lblmusteri = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.Btnparacekme, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.Btnkart, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.Btnyatirma, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.Btntransfer, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.Btngiris, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.Btnhesap, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 131);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(287, 158);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // Btnparacekme
            // 
            this.Btnparacekme.Location = new System.Drawing.Point(146, 3);
            this.Btnparacekme.Name = "Btnparacekme";
            this.Btnparacekme.Size = new System.Drawing.Size(138, 47);
            this.Btnparacekme.TabIndex = 1;
            this.Btnparacekme.Text = "PARA ÇEKME";
            this.Btnparacekme.UseVisualStyleBackColor = true;
            this.Btnparacekme.Click += new System.EventHandler(this.Btnparacekme_Click);
            // 
            // Btnkart
            // 
            this.Btnkart.Location = new System.Drawing.Point(3, 56);
            this.Btnkart.Name = "Btnkart";
            this.Btnkart.Size = new System.Drawing.Size(137, 47);
            this.Btnkart.TabIndex = 2;
            this.Btnkart.Text = "KART İŞLEMLERİ";
            this.Btnkart.UseVisualStyleBackColor = true;
            this.Btnkart.Click += new System.EventHandler(this.Btnkart_Click);
            // 
            // Btnyatirma
            // 
            this.Btnyatirma.Location = new System.Drawing.Point(146, 56);
            this.Btnyatirma.Name = "Btnyatirma";
            this.Btnyatirma.Size = new System.Drawing.Size(138, 47);
            this.Btnyatirma.TabIndex = 3;
            this.Btnyatirma.Text = "PARA YATIRMA";
            this.Btnyatirma.UseVisualStyleBackColor = true;
            this.Btnyatirma.Click += new System.EventHandler(this.Btnyatirma_Click);
            // 
            // Btntransfer
            // 
            this.Btntransfer.Location = new System.Drawing.Point(3, 109);
            this.Btntransfer.Name = "Btntransfer";
            this.Btntransfer.Size = new System.Drawing.Size(137, 46);
            this.Btntransfer.TabIndex = 4;
            this.Btntransfer.Text = "PARA TRANSFERİ";
            this.Btntransfer.UseVisualStyleBackColor = true;
            this.Btntransfer.Click += new System.EventHandler(this.Btntransfer_Click);
            // 
            // Btngiris
            // 
            this.Btngiris.Location = new System.Drawing.Point(146, 109);
            this.Btngiris.Name = "Btngiris";
            this.Btngiris.Size = new System.Drawing.Size(138, 46);
            this.Btngiris.TabIndex = 5;
            this.Btngiris.Text = "GİRİŞ MENÜSÜ";
            this.Btngiris.UseVisualStyleBackColor = true;
            this.Btngiris.Click += new System.EventHandler(this.Btngiris_Click);
            // 
            // Btnhesap
            // 
            this.Btnhesap.Location = new System.Drawing.Point(3, 3);
            this.Btnhesap.Name = "Btnhesap";
            this.Btnhesap.Size = new System.Drawing.Size(137, 47);
            this.Btnhesap.TabIndex = 0;
            this.Btnhesap.Text = "HESAP İŞLEMLERİ";
            this.Btnhesap.UseVisualStyleBackColor = true;
            this.Btnhesap.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.Lblsoyad);
            this.groupBox1.Controls.Add(this.Lblad);
            this.groupBox1.Controls.Add(this.Lblmusteri);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(284, 113);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "HESAP BİLGİLERİ";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(142, 73);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 8;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(142, 50);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 7;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(142, 24);
            this.textBox1.MaxLength = 10;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(93, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(10, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = ":";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(93, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(10, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = ":";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(93, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(10, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = ":";
            // 
            // Lblsoyad
            // 
            this.Lblsoyad.AutoSize = true;
            this.Lblsoyad.Location = new System.Drawing.Point(6, 76);
            this.Lblsoyad.Name = "Lblsoyad";
            this.Lblsoyad.Size = new System.Drawing.Size(44, 13);
            this.Lblsoyad.TabIndex = 2;
            this.Lblsoyad.Text = "SOYAD";
            // 
            // Lblad
            // 
            this.Lblad.AutoSize = true;
            this.Lblad.Location = new System.Drawing.Point(6, 50);
            this.Lblad.Name = "Lblad";
            this.Lblad.Size = new System.Drawing.Size(22, 13);
            this.Lblad.TabIndex = 1;
            this.Lblad.Text = "AD";
            // 
            // Lblmusteri
            // 
            this.Lblmusteri.AutoSize = true;
            this.Lblmusteri.Location = new System.Drawing.Point(6, 23);
            this.Lblmusteri.Name = "Lblmusteri";
            this.Lblmusteri.Size = new System.Drawing.Size(75, 13);
            this.Lblmusteri.TabIndex = 0;
            this.Lblmusteri.Text = "MÜŞTERİ NO";
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(311, 301);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Form3";
            this.Text = "HESAP BİLGİLERİ";
            this.Load += new System.EventHandler(this.Form3_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button Btnparacekme;
        private System.Windows.Forms.Button Btnkart;
        private System.Windows.Forms.Button Btnyatirma;
        private System.Windows.Forms.Button Btntransfer;
        private System.Windows.Forms.Button Btngiris;
        private System.Windows.Forms.Button Btnhesap;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Lblsoyad;
        private System.Windows.Forms.Label Lblad;
        private System.Windows.Forms.Label Lblmusteri;
    }
}