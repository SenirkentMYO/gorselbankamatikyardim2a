﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GorselBankamatikYardim2A
{
    public partial class subelerimiz : Form
    {
        public subelerimiz()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (gorselDBEntities database = new gorselDBEntities())
            {

                List<sehir> list = database.sehir.Where(tablom => tablom.Adi.Contains(txtsonuc.Text)).ToList();

                dtgrid.DataSource = list;
            }
        }

        private void dtgrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void subelerimiz_Load(object sender, EventArgs e)
        {
            using (gorselDBEntities database = new gorselDBEntities())
            {
                var sorgu = database.sehir.Select(a => new { ıd = a.ID, Adi = a.Adi }).ToList();
                dtgrid.DataSource = sorgu;
            }
        }

        private void btnduzenle_Click(object sender, EventArgs e)
        {

            gorselDBEntities db = new gorselDBEntities();
            int id = int.Parse(dtgrid.SelectedRows[0].Cells[0].Value.ToString());
            sehir shr = db.sehir.Where(a => a.ID == id).First();

            sehirduzenle f = new sehirduzenle(shr);
            f.Show();
        }
    }
}
