﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GorselBankamatikYardim2A
{
    public partial class hesapislemleri : Form
    {
        public hesapislemleri()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form3 geri = new Form3();
            geri.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string connectionString = "Data Source= MUZO\SQLEXPRESS;Initial Catalog=gorselDB;User ID=12345;Password=123e";
            SqlConnection Connection = new SqlConnection(connectionString);
            if (Connection.State == ConnectionState.Closed)
            {
                Connection.Open();
            }
            string commandText = string.Format("update Categories set CategoryName='{0}',Description='{1}'  where CategoryID='{2}'", textBox1.Text, textBox2.Text);
            SqlCommand command = new SqlCommand(commandText, Connection);

            int sonuc = command.ExecuteNonQuery();
            if (sonuc > 0)
            {
                MessageBox.Show("Güncelleme işleminiz başarıyla gerçekleşti..");
            }
            else
            {
                MessageBox.Show("güncelleme işleminiz başarısızdır..");
            }

            Connection.Close();
            command.Dispose();
        }
    }
}
