﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GorselBankamatikYardim2A
{
    public partial class kartislemleri : Form
    {
        public kartislemleri()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            sifredegistir hsp = new sifredegistir();
            hsp.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            hesapbakiye hsp = new hesapbakiye();
            hsp.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            g_rapor grapor = new g_rapor();
            grapor.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            a_rapor arapor = new a_rapor();
            arapor.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Yıllık yıl = new Yıllık();
            yıl.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Form3 geri = new Form3();
            geri.Show();
            this.Close();
        }
    }
}
