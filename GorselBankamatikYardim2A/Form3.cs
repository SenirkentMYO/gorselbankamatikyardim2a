﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GorselBankamatikYardim2A
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            hesapislemleri hsp = new hesapislemleri();
            hsp.Show();

        }

        private void Btnparacekme_Click(object sender, EventArgs e)
        {
            paracekme pra = new paracekme();
            pra.Show();

        }

        private void Btnkart_Click(object sender, EventArgs e)
        {
            kartislemleri kislem = new kartislemleri();
            kislem.Show();
        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Btntransfer_Click(object sender, EventArgs e)
        {
            paratransferi trans = new paratransferi();
            trans.Show();

            this.Close();
        }

        private void Btnyatirma_Click(object sender, EventArgs e)
        {
            parayatirma yatir = new parayatirma();
            yatir.Show();
        }

        private void Btngiris_Click(object sender, EventArgs e)
        {
            Form1 grs = new Form1();
            grs.Show();

            this.Close();
        }
    }
}
