﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GorselBankamatikYardim2A
{
    public partial class paracekme : Form
    {
        public paracekme()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form3 geri = new Form3();
            geri.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            hesap hsp = new hesap();
            hsp.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            kart krt = new kart();
            krt.Show();
        }
    }
}
